document.addEventListener('DOMContentLoaded', () => {
  const input = document.getElementById('word-input');
  const button = document.getElementById('search-button');
  const synonymsList = document.getElementById('synonyms');
  const synonymsText = document.getElementById('synonyms-text');

  // Function to fetch synonyms and display them
  const fetchSynonyms = async () => {
    const word = input.value.trim();
    if (word !== '') {
      try {
        const response = await fetch(
          `https://server.harka.com/szinonima/${encodeURIComponent(word)}`
        );
        if (response.ok) {
          const data = await response.text();
          const parser = new DOMParser();
          const xmlDoc = parser.parseFromString(data, 'application/xml');
          const szinonimak = xmlDoc.getElementsByTagName('szinonimak')[0];
          const szocsoport = szinonimak.getElementsByTagName('szocsoport')[0];
          const synonyms = szocsoport.getElementsByTagName('szinonima');

          if (synonyms.length > 0) {
            synonymsList.innerHTML = '';
            for (let i = 0; i < synonyms.length; i++) {
              const synonym = synonyms[i].textContent;
              const li = document.createElement('li');
              li.textContent = synonym;
              synonymsList.appendChild(li);
            }
            synonymsText.style.display = 'none';
            synonymsList.style.display = 'block';
          } else {
            synonymsText.textContent = 'No synonyms found.';
            synonymsText.style.display = 'block';
            synonymsList.style.display = 'none';
          }
        } else {
          throw new Error('Error fetching synonyms.');
        }
      } catch (error) {
        console.error('Error:', error);
        synonymsText.textContent = 'Error fetching synonyms.';
        synonymsText.style.display = 'block';
        synonymsList.style.display = 'none';
      }
    } else {
      synonymsText.style.display = 'block';
      synonymsList.style.display = 'none';
    }
  };

  // Event listeners for button click and input field enter key press
  button.addEventListener('click', fetchSynonyms);

  input.addEventListener('keydown', event => {
    if (event.key === 'Enter') {
      fetchSynonyms();
    }
  });
});
